package com.crosley.util;
import com.crosley.beans.Items;
import com.crosley.beans.ShoppingCart;
import com.crosley.beans.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory(){
        if(sessionFactory == null){
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
            settings.put(Environment.URL,"jdbc:sqlserver://chriscross.database.windows.net:1433;databaseName=training-db");
            settings.put(Environment.USER, "ChrisCross@chriscross");
            settings.put(Environment.PASS, "myAzureAcct321!" );

            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "validate");
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            //privide hibernate mappings to configuration
            configuration.addAnnotatedClass(Items.class);
            configuration.addAnnotatedClass(Users.class);
            configuration.addAnnotatedClass(ShoppingCart.class);

            sessionFactory = configuration.buildSessionFactory();

        }
        return sessionFactory;
    }

    public static Session getSession(){
        return getSessionFactory().openSession();
    }


}
