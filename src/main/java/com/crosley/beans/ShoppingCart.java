package com.crosley.beans;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class ShoppingCart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int shoppingCartId;
    @OneToOne
    @JoinColumn(name = "userId")
    Users users;
    Items items;

    public ShoppingCart(int shoppingCartId, Users users, Items itemsList) {
        this.shoppingCartId = shoppingCartId;
        this.users = users;
        this.items = items;
    }

    public ShoppingCart() {
    }

    public int getShoppingCartId() {
        return shoppingCartId;
    }

    public void setShoppingCartId(int shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Items getItems() {
        return items;
    }

    public void setItems(Items items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCart that = (ShoppingCart) o;
        return shoppingCartId == that.shoppingCartId && Objects.equals(users, that.users) && Objects.equals(items, that.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shoppingCartId, users, items);
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "shoppingCartId=" + shoppingCartId +
                ", users=" + users +
                ", items=" + items +
                '}';
    }
}
