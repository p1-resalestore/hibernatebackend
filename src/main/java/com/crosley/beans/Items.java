package com.crosley.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Items implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int itemId;

    private String itemPicture;
    private String itemName;
    private String itemCategory;
    private String itemCondition;
    private String itemDescription;
    private int itemQuantity;
    private double itemPrice;

    public Items() {
    }

    public Items(int itemId, String itemPicture, String itemName, String itemDescription, String itemCategory, String itemCondition, int itemQuantity, double itemPrice) {
        this.itemId = itemId;
        this.itemPicture = itemPicture;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemCategory = itemCategory;
        this.itemCondition = itemCondition;
        this.itemQuantity = itemQuantity;
        this.itemPrice = itemPrice;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemCondition() { return itemCondition; }

    public void setItemCondition(String itemCondition) { this.itemCondition = itemCondition; }

    public String getItemCategory() { return itemCategory; }

    public void setItemCategory(String itemCategory) { this.itemCategory = itemCategory; }

    public int getItemQuantity() { return itemQuantity; }

    public void setItemQuantity(int itemQuantity) { this.itemQuantity = itemQuantity; }

    public String getPicture() { return itemPicture; }

    public void setPicture(String picture) { this.itemPicture = picture; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Items items = (Items) o;
        return itemId == items.itemId && itemQuantity == items.itemQuantity && Double.compare(items.itemPrice, itemPrice) == 0 && Objects.equals(itemPicture, items.itemPicture) && Objects.equals(itemName, items.itemName) && Objects.equals(itemDescription, items.itemDescription) && Objects.equals(itemCategory, items.itemCategory) && Objects.equals(itemCondition, items.itemCondition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, itemPicture, itemName, itemDescription, itemCategory, itemCondition, itemQuantity, itemPrice);
    }

    @Override
    public String toString() {
        return "Items{" +
                "itemId=" + itemId +
                ", itemPicture='" + itemPicture + '\'' +
                ", itemName='" + itemName + '\'' +
                ", itemCategory='" + itemCategory + '\'' +
                ", itemCondition='" + itemCondition + '\'' +
                ", itemDescription='" + itemDescription + '\'' +
                ", itemQuantity=" + itemQuantity +
                ", itemPrice=" + itemPrice +
                '}';
    }
}
