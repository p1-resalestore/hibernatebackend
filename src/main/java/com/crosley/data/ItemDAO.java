package com.crosley.data;

import com.crosley.beans.Items;

import java.util.List;

public interface ItemDAO {

    public List<Items> getAllItems();
    public Items getItemById(int id);
    public Items addNewItem(Items item);
    public void removeItem(int id);
    public Items updateItem(Items item);

}
