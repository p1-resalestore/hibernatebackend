package com.crosley.data;

import com.crosley.beans.Items;
import com.crosley.beans.Users;
import com.crosley.util.HibernateUtil;
import org.hibernate.Session;



import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UsersDAOHibImpl implements UserDAO{
    Logger log = LoggerFactory.getLogger(UsersDAOHibImpl.class);
    @Override
    public List<Users> getAllUsers() {
        try(Session s = HibernateUtil.getSession();) {
            //using HQL to query database...from Items same as "select * from items"
            List<Users> users = s.createQuery("from Users", Users.class).list();
            return users;
        }
    }

    @Override
    public Users getUserById(int id) {
        try(Session s = HibernateUtil.getSession();) {
            Users users = s.createQuery("from Users where userId = " + id, Users.class).getSingleResult();
            return users;
        }
    }

    @Override
    public Users getUserByUsername(String name) {
        try(Session s = HibernateUtil.getSession()) {
           Users thisName = s.createQuery("from Users where username = :name"  , Users.class)
                   .setParameter("name", name)
                   .getSingleResult();
           log.info(String.valueOf(thisName));

            return thisName;
        }
    }

    @Override
    public Users addNewUser(Users users) {
         try(Session s = HibernateUtil.getSession();) {
            //using HQL to query database...from Items same as "select * from items"
            s.beginTransaction();
            Users user = new Users();
             user.setUserRole(users.getUserRole());
             user.setUsername(users.getUsername());
             user.setUserpass(users.getUserpass());
             user.setFirstname(users.getFirstname());
             user.setLastname(users.getLastname());
             user.setAddress(users.getAddress());
             user.setEmail(users.getEmail());
            s.save(user);
            s.getTransaction().commit();

            return user;
        }
    }

    @Override
    public Users updateUser(Users users) {
        try(Session s = HibernateUtil.getSession()) {
            s.beginTransaction();
            s.update(users);
            s.getTransaction().commit();
        }
        return users;
    }

    @Override
    public void removeUser(int id) {
        log.info(String.valueOf(id));
        try(Session s = HibernateUtil.getSession()){
            s.beginTransaction();
            Users users = getUserById(id);
            log.info(String.valueOf(users));
            s.delete(users);
            s.getTransaction().commit();
        }

    }
}
