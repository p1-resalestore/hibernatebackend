package com.crosley.data;

import com.crosley.beans.Users;

import java.sql.SQLException;
import java.util.List;

public interface UserDAO {
     List<Users> getAllUsers();
     Users getUserById(int id);
     Users getUserByUsername(String name);
     Users addNewUser(Users users);
     Users updateUser(Users users);
     void removeUser(int id);
}
