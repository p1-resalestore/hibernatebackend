package com.crosley.services;

import com.crosley.beans.Users;
import com.crosley.data.UsersDAOHibImpl;


import java.util.List;

public class UserService {
    UsersDAOHibImpl userDAOHibImpl = new UsersDAOHibImpl();

    public List<Users> getUsers() {return userDAOHibImpl.getAllUsers(); }
    public Users getUser(int id) {return userDAOHibImpl.getUserById(id);}
    public Users addUser(Users users) { return userDAOHibImpl.addNewUser(users);}
    public Users updateUser(Users users){return userDAOHibImpl.updateUser(users);}
    public void removeUser(int id ) {userDAOHibImpl.removeUser(id);}
    public Users getByUserName(String username){ return userDAOHibImpl.getUserByUsername(username);};
}
