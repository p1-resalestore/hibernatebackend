package com.crosley.controllers;


import com.crosley.beans.Items;
import com.crosley.customexceptions.wrongInput;
import com.crosley.services.ItemService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;


public class ItemController {
    Logger log = LoggerFactory.getLogger(ItemController.class);
    private ItemService itemService = new ItemService();

//getting an item bu id
    public void handleGetItemById(Context ctx) {
        //int
        String idString = ctx.pathParam("itemId");
        log.info(idString);
        if(idString.matches("^\\d+$")){
            int idInt = Integer.parseInt(idString);
            Items item = itemService.getItem(idInt);
            if(item == null){
                throw new NotFoundResponse("Item not found");
            }else{
                ctx.json(item);
                log.info(String.valueOf(item));
                //return item;
               // item.getItemId();
            }
        }else{
            throw new NotFoundResponse("Must enter a numeric value");
        }
    }
//getting all items in database
    public void handleGetAllItems(Context ctx){
        ctx.json(itemService.getAll());
    }
//adding an item to database
    public void handlePostNewItem(Context ctx){
        if(ctx.method().equals("OPTIONS")){
            return;
        }
       Items item = ctx.bodyAsClass(Items.class);
        itemService.addItem(item);
    }
//updates item in database by id
//to update an item copy an existing item into a post body
//with the URL localhost:7000/items/1/update. Change the 1 to the
//id of the item you would like to update and change the fields
//that need to be updated
    public void handlePostUpdateItem(Context ctx) {
            Items item = ctx.bodyAsClass(Items.class);
            log.info(String.valueOf(item));
            itemService.updateItem(item);
        }

//removing an item from database by id
    public void handlePostRemoveItemById(Context ctx){
       String id = ctx.body();
       int idInt = Integer.parseInt(id);
        itemService.removeItem(idInt);
        }
    }

