
#sets up environment ,to run app in
FROM java:8
#copy takes 2 params: host container location, 2nd location in container
COPY build/libs/Project0_ResaleStore-1.0-SNAPSHOT.jar .
EXPOSE   80
#run and CMD run executable code
CMD java -jar Project0_ResaleStore-1.0-SNAPSHOT.jar
